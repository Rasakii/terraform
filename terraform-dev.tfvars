cidr_blocks = [
    {cidr_block = "10.0.0.0/16", name = "Development" },
    {cidr_block = "10.0.10.0/24", name = "Subnet-1-dev"}
]

environment = "Development"